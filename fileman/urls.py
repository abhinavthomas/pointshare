"""fileman URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from web.views import index
from web.views import login
from web.views import logout
from web.views import about
from web.views import panel
from web.views import send
from web.views import downloadenc
from web.views import loginext
from web.views import decrypt
from web.views import panel_ext
from web.views import down_at

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', index),
    url(r'^login/',login),
    url(r'^logout/',logout),
    url(r'^about/$',about),
    url(r'^panel/$',panel),
    url(r'^send/$',send),
    url(r'^downloadenc/$',downloadenc),
    url(r'^loginext/$',loginext),
    url(r'^decrypt/$',decrypt),
    url(r'^panel_ext/$',panel_ext),
    url(r'^downloadattachment/([0-9]{1,})/$', down_at),
    
]
