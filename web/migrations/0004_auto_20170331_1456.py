# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-31 14:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0003_auto_20170331_1420'),
    ]

    operations = [
        migrations.AlterField(
            model_name='auth_table',
            name='key_pair',
            field=models.CharField(default=b'-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAmM1W+NsxZKBKorn4CAt886+aXvbhhs98JjmyyWH1gt+qGx7X\nQ8YzS7hOXrn33iR+PsEY6Gqwv1/J2sp7ZFXr1lYcknd6gMrO8h1OMKcuwtdBqMcn\nia6yBFTMz9Y6W/eWRL0Xu0Brpk3RRq7Z1KzcL4SxOLwVvpcV2W1yIkUNxiqVIgxS\n/RtLwuOFD8Lz4IU9zNKzGTbPEdndZikHnmwGb+BWH64En21dWhS6U+Vr0whe93Kz\nc1vOMpRZKVJG2EyNtY/WAcuN5CaUIm53zggj7Hyaqdecrx1KH12zhNb3G7gtXyt3\nubbd9Qrf0XY49I2utSUsS8YA4yXX7Dz3UFZ9FQIDAQABAoIBAQCGBFMroNQJda0A\nXCt2otzMvk+eytmy63fv7tsoVYqHZrRDZSFYiBWla9kQq/sCEu/HCoVWE31B3Dmm\nc8EVl2Zpbs1QiQogu8AWCi3x7i/NJA6qgUHkOBxaQCYCg9pbnyNk1pRh33aLPeU3\nIVRce+ACrVbic9R+fPTk5mEcPYR/MK5/NwYvzVEqFv+KTOZdDQJr7ODOML2dqJca\nyUN/fIT2yJjKTSSUTEhxRDi18zU5uX1QHBK9M6Ibwqo21CFeOZMZgqZN3SciUWyc\nlihNJRPwc2wVlYEOhqL45bwtxHnPJy5j1vMBQDQOO26s6bJDv0wEn7xI+0IjrzKx\n14EKn41hAoGBALuPPUQ8boC4ykdC3uJjomKDnRIzS2IeRwoLOLQSYp0ybGRmCP4h\nWx0ZmDDC0rwmFfV2t8JCQWm4iFGeo1RGfq5Ea13/gXPFU59tQk1RcTzkrss6taxx\nh4DAqWGVnAFznS/MoOLuZNbRFnw977m9+hq4ptU6jlcYJEJdZDPEOXYvAoGBANCP\nQcLwDAb4rMbvASOmfJn48E644G9GRzvOhSjb9Exmnb1oYd5CkNKPB4U1zsMAJe/k\nKrM51vLI6PqQ2s6SroxgDNjZMWNpyWBRihB2VI4S7FxGL9cRZ9FIJcc/bhcYzEuW\nlFA0vBL5/CuHcfpTUqBC2Hq6vvile2jfpTHY//P7AoGAdXpFnKzhZfzYrgPjiRdx\nCZV4V4MLb3yZcGAQ9zEg1WdU3xnN/ic0Y6i5W6lbx39vd50RgjpvsGFl+QGNOech\nx9OREIJOOg5Zz1znD3i4nbR7C05NpqJBhlKmhTnpSjt5NAxOpXU+chLqq407zzKA\nKiaIULxplYHIjEzky0vX1f8CgYEAwmUlKKxSqZTUhsBz0WT3MxVD+Dx+hXl+IBLJ\nL2KFfUkQsmE/B8oH7xr+KX4T8NKMieOkoVXCMp5t+k9Wg8FZDOLiVqVrUXxjBXI7\nPEjvW1XsFb4eDVrd58ZV4DgRR1oH4lTCfVgKDa4RDAm9hVRw6Ywk95BlPKISO5hq\nA+J+AE0CgYBtefqOkTT4MjUxlHVMisBMYQamOicDmmKoYgZyUSk8HAdnjQu/WrlI\nYGS5CagYG0Gh9yDAGnThjtjuEz7Q11iqqAr3ptDYmVf7BjywnTHq+uD9sdwK2Ewh\nZMk8ey/EmFytC3UY1VOGoEz+N0ClEC7BYIBRSGgbTlLmTFoK7vBjWA==\n-----END RSA PRIVATE KEY-----', max_length=50, unique=True),
        ),
        migrations.AlterField(
            model_name='auth_table',
            name='public_key',
            field=models.CharField(default=b'-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmM1W+NsxZKBKorn4CAt8\n86+aXvbhhs98JjmyyWH1gt+qGx7XQ8YzS7hOXrn33iR+PsEY6Gqwv1/J2sp7ZFXr\n1lYcknd6gMrO8h1OMKcuwtdBqMcnia6yBFTMz9Y6W/eWRL0Xu0Brpk3RRq7Z1Kzc\nL4SxOLwVvpcV2W1yIkUNxiqVIgxS/RtLwuOFD8Lz4IU9zNKzGTbPEdndZikHnmwG\nb+BWH64En21dWhS6U+Vr0whe93Kzc1vOMpRZKVJG2EyNtY/WAcuN5CaUIm53zggj\n7Hyaqdecrx1KH12zhNb3G7gtXyt3ubbd9Qrf0XY49I2utSUsS8YA4yXX7Dz3UFZ9\nFQIDAQAB\n-----END PUBLIC KEY-----', max_length=50, unique=True),
        ),
    ]
