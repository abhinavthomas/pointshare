from __future__ import unicode_literals

from django.db import models
from datetime import datetime
import Crypto
from Crypto.PublicKey import RSA
from Crypto.Hash import MD5
from datetime import datetime
def get_pri():
    key_pair = RSA.generate(2048)
    return key_pair.exportKey()

class Auth_table(models.Model):

    mail = models.EmailField(unique = True)
    private_key = models.BinaryField(max_length=5000,unique = True,default = 'private_key comes here')
    public_key = models.BinaryField(max_length=5000,unique = True,default = 'public_key comes here')
    time = models.DateTimeField(default = datetime.now)
    blacklisted = models.BooleanField(default = False)

    def save(self, *args, **kwargs):
        key_pair = RSA.generate(2048)
        self.private_key = key_pair.exportKey()
        self.public_key = key_pair.publickey().exportKey()
        self.time = datetime.now()
        models.Model.save(self, *args,**kwargs)


class Log(models.Model):
    mail = models.EmailField()
    time = models.DateTimeField()
    authd = models.BooleanField(default = False)
    info = models.TextField(max_length=5000,null = True)

class enc_log(models.Model):
    CHOICES = (
        ('E', 'Encrypted'),
        ('D', 'Decrypted'),
    )
    filename = models.CharField(max_length=50)
    mail = models.EmailField()
    enc_time = models.DateTimeField(default = datetime.now)
    e_d = models.CharField(max_length=1, choices=CHOICES)