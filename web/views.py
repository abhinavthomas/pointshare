from django.shortcuts import render
from models import Auth_table,Log,enc_log
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from datetime import datetime
from django.db import IntegrityError
from smtplib import SMTPAuthenticationError
from django.http import HttpResponse,HttpResponseRedirect
from smtplib import SMTP
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders
from django import forms
import os, random, struct
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
import zipfile
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db import models
from email import parser
import email
import imaplib
from django.contrib.auth import authenticate, login,logout

class Document(models.Model):
    mail_id = models.EmailField()
    document = models.FileField(upload_to='documents/')
    subject = models.CharField(max_length=50)

class SendForm(forms.ModelForm):
    class Meta:
            model = Document
            fields = ('mail_id', 'document','subject',)

class NameForm(forms.Form):
    username = forms.EmailField(label='mailid', max_length=100)
    password = forms.CharField(label='Your name', max_length=100)

def logout(request):
    request.session['auth'] = False
    return HttpResponseRedirect('/')

def index(request):
    try:
        if request.session['auth']==True:
            return HttpResponseRedirect('/panel/')
        else:
            return render(request,'index.html',{})
    except Exception as e:
        return render(request,'index.html',{})
    


def encrypt_file(in_file, rec_key, out_filename=None, chunksize=64*1024):
    key = os.urandom(32)
    out_filename = in_file.name + '.enc'
    iv = os.urandom(16)
    encryptor = AES.new(key, AES.MODE_CBC, iv)
    filesize = in_file.size
    print in_file.name
    print out_filename
    print filesize
    with open(out_filename, 'wb') as outfile:
        outfile.write(struct.pack('<Q', filesize))
        outfile.write(iv)
        while True:
            chunk = in_file.read(chunksize)
            if len(chunk) == 0:
                break
            elif len(chunk) % 16 != 0:
                chunk += ' '.encode() * (16 - len(chunk) % 16)
            outfile.write(encryptor.encrypt(chunk))
        pub_key = RSA.importKey(rec_key)
        enc = pub_key.encrypt(key, 32)
        with open("key",'wb') as k:
            for i in enc:
                k.write(i)
    name=in_file.name+'_pointshare.zip'
    with zipfile.ZipFile(name, 'w') as myzip:
        myzip.write(out_filename)
        myzip.write('key')
    return name 


@csrf_protect
def downloadenc(request):
    if request.method == 'POST':
        if request.session['auth'] == True:
            username = request.session['uname']
            password = request.session['passwd']
            try:
                file = request.FILES['file']
                email = request.POST.get("email", "default")
                sub = request.POST.get("message", "default")
                file = request.FILES.get("file")
                pub_key = Auth_table.objects.get(mail = email).public_key
            except Exception as e:
                return render(request,'index.html',{'obj': 'Failed Transaction'})
            
            k=encrypt_file(file,pub_key)
            obj = enc_log(filename = file.name,mail = username,enc_time = datetime.now(),e_d = 'E')
            obj.save()
                

            fh = open(k,'rb')
            response = HttpResponse(fh.read(), content_type="application/zip")
            response['Content-Disposition'] = 'inline; filename=%s'%k
            return response

@csrf_protect
def send(request):
    if request.method == 'POST':
        if request.session['auth'] == True:
            username = request.session['uname']
            password = request.session['passwd']
            file = request.FILES['file']
            email = request.POST.get("email", "default")
            print email
            sub = request.POST.get("message", "default")
            print sub
            file = request.FILES.get("file")
            print type(file)
            msg = MIMEMultipart()
            msg['From'] = username
            msg['To'] = email
            msg['Subject'] = sub
            body = "Sent via PointShare"
            msg.attach(MIMEText(body, 'plain'))
            try:
                pub_key = Auth_table.objects.get(mail = email).public_key
            except Exception as e:
                return HttpResponseRedirect('/panel/')
            k = encrypt_file(file,pub_key)
            obj = enc_log(filename = file.name,mail = username,enc_time = datetime.now(),e_d = 'E')
            obj.save()    
            attachment = open(k,'rb')
            part = MIMEBase('application','zip')
            part.set_payload((attachment).read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', "attachment; filename= %s"%k)
            msg.attach(part)
            server = SMTP('smtp.gmail.com:587')
            server.starttls()
            server.login(username,password)
            text = msg.as_string()
            server.sendmail(username, email, text)
            server.quit()
            return HttpResponseRedirect('/panel/')

        else :
            return HttpResponseRedirect('/')

    else:
        return HttpResponseRedirect('/panel/')        

def panel_ext(request):
    try:
        if request.session['auth'] == True:
            return render(request,'panel_ext.html',{}) 
        else:
            return render(request,'index_ext.html',{'obj': 'login failed'})
    except Exception as e:
        return render(request,'index_ext.html',{'obj': 'login failed'})

    
@csrf_protect
def loginext(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        print username
        password = request.POST.get('password')
        print password
        try:
            print "before auth"
            user = authenticate(username=username, password=password)
            request.session['auth'] = True
            if user is not None:
                print "login completed"
                
                try:
                    USERNAME = username + '@external.com'
                    print USERNAME
                    obj = Auth_table(mail = USERNAME)
                    obj.save()
                    print "object saved"
                except IntegrityError as e:
                    print "inside integrity error"
                    emp = Auth_table.objects.get(mail = USERNAME)
                    print "mail : " + emp.mail
                    print "Blacked : "+ str(emp.blacklisted)
                    print "hello"
                    if emp.blacklisted == True:
                        print "blacklisted"
                        request.session['auth'] = False
                        return render(request,'index_ext.html',{'obj': 'login failed'})
                    else :
                        print "inside else"
                        request.session['auth'] = True
                        print "authenticated : True"
                        request.session['uname'] = USERNAME
                        request.session['passwd'] = password
                        return HttpResponseRedirect('/panel_ext/')    
                else:
                    return HttpResponseRedirect('/panel_ext/')    
            else:
                return render(request,'index_ext.html',{'obj': 'login failed'})
        except Exception as e:
            print e
            print "Exception"
            request.session['auth'] = False
            return render(request,'index_ext.html',{'obj': 'login failed'}) 
        
    else :
        return render(request,'index_ext.html',{}) 

@csrf_protect
def login(request):
    if request.method == 'POST':
        form = NameForm(request.POST)
        
        if form.is_valid():
            data = form.cleaned_data

            USERNAME = data['username']
            PASSWORD = data['password']
            conn = SMTP("smtp.gmail.com:587")
            conn.starttls()
            conn.set_debuglevel(False)
            request.session['auth'] = False
            try:
                conn.login(USERNAME,PASSWORD)
                try:
                    obj = Auth_table(mail = USERNAME)
                    obj.save()
                except IntegrityError as e:
                    emp = Auth_table.objects.get(mail = USERNAME)
                    if emp.blacklisted == True:
                        request.session['auth'] = False
                        return render(request,'index.html',{'obj': 'login failed'})
                    else :
                        request.session['auth'] = True
                        request.session['uname'] = USERNAME
                        request.session['passwd'] = PASSWORD
                        return HttpResponseRedirect('/panel/')

                except Exception as e:                
                    request.session['auth'] = False
                    return render(request,'index.html',{'obj': 'login failed'})

                request.session['auth'] = True
                request.session['uname'] = USERNAME
                request.session['passwd'] = PASSWORD
                return HttpResponseRedirect('/panel/')

            except SMTPAuthenticationError as e:
                request.session['auth'] = False
                return render(request,'index.html',{'obj': 'login failed'})
            finally :
                log_obj = Log(mail = USERNAME,authd = request.session['auth'],time = datetime.now() ,info = 'Tried logging in')
                log_obj.save()
                conn.quit()
    else:
        return HttpResponseRedirect('/')

    return HttpResponseRedirect('/')

def decrypt_file(in_fil,prikey, out_filename=None, chunksize=24*1024):
    in_filename = in_fil.name
    k=open(in_fil.name,'wb')
    k.write(in_fil.read())
    k.close()
    print in_filename
    with zipfile.ZipFile(in_filename, 'r') as myzip:
        z=myzip.infolist()
        myzip.extractall()
    for i in z:
        if i.filename!='key':
            in_filename =i.filename
    # print(in_filename)
    f1 = open("key", "rb")
    input = f1.read()
    pri_key = RSA.importKey(prikey)
    key = pri_key.decrypt(input)
    out_filename = in_filename.strip('.enc')
    print out_filename
    with open(in_filename, 'rb') as infile:
        origsize = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
        iv = infile.read(16)
        try:
            decryptor = AES.new(key, AES.MODE_CBC, iv)
        except Exception as e:
                return -1        

        with open(out_filename, 'wb') as outfile:
            while True:
                chunk = infile.read(chunksize)
                if len(chunk) == 0:
                    break
                outfile.write(decryptor.decrypt(chunk))
            outfile.truncate(origsize)
        return out_filename

@csrf_protect
def decrypt(request):
    if request.method == 'POST':
        if request.session['auth'] == True:
            username = request.session['uname']
            password = request.session['passwd']
            try:
                file = request.FILES['file']
                private_key = Auth_table.objects.get(mail = username).private_key
                print file.name
                filename = decrypt_file(file,private_key)
                obj = enc_log(filename = file.name,mail = username,enc_time = datetime.now(),e_d = 'D')
                obj.save()
                if filename == -1:
                    request.session['auth']=False
                    return render(request,'index.html',{'obj': 'Failed Transaction'})
                fh = open(filename,'rb')
                response = HttpResponse(fh.read(), content_type="application/zip")
                response['Content-Disposition'] = 'inline; filename=%s'%filename
                return response
            except Exception as e:
                return render(request,'index.html',{'obj': 'Failed Transaction'})
    else:
        return render(request,'index.html',{'obj': 'Failed Transaction'})       


def panel(request):
    try:
        if request.session['auth'] == True:
            bl = imaplib.IMAP4_SSL('imap.gmail.com', 993)
            username = request.session['uname']
            password = request.session['passwd']
            try:
                bl.login(username, password)
            except Exception as e:
                return HttpResponseRedirect('/panel_ext/')
            bl.select('inbox')
            result, data = bl.search(None, "ALL")
            ids = data[0]
            id_list = ids.split()
            obj = [{}]
            for i in range (-1, max(-15, -len(id_list)), -1):
                result, data = bl.fetch(id_list[i], "(RFC822)")
                raw_email = data[0][1]
                msg = email.message_from_string(raw_email)
                if msg.is_multipart():
                    if(msg.get_payload(0) and str(msg.get_payload(0)).find('Sent via PointShare') != -1):
                        obj.append({'id': id_list[i], 'from': msg['From'], 'subject': msg['Subject'], 'date': msg['Date']})
            print obj
            obj=obj[1:]
            bl.close()
            bl.logout()
            return render(request,'panel.html',{'obj': obj})
        else :
            return HttpResponseRedirect('/')

    except KeyError as e:
        return HttpResponseRedirect('/')

def about(request):
    return render(request,'about.html',{})

def down_at(request, id):
    try:
        if request.session['auth'] == True:
            bl = imaplib.IMAP4_SSL('imap.gmail.com', 993)
            username = request.session['uname']
            password = request.session['passwd']
            print "hello"
            bl.login(username, password)
            print '2'
            private_key = Auth_table.objects.get(mail = username).private_key
            bl.select('inbox')
            print private_key
            result, data = bl.fetch(id, "(RFC822)")
            raw_email = data[0][1]
            print raw_email
            msg = email.message_from_string(raw_email)
            file = msg.get_payload(1)
            filename = msg.get_payload(1).get_filename()
            print "File name : " +filename

            f=open(filename, 'wb')
            print "file opened"
            f.write(file.get_payload(decode=True))
            f.close()
            print "file closed"
            obj = enc_log(filename = filename,mail = username,enc_time = datetime.now(),e_d = 'D')
            obj.save()    
            print type(obj)
            print "inside downloadattach", filename
            #x=decrypt_file2(filename,private_key)
            with zipfile.ZipFile(filename, 'r') as myzip:
                z=myzip.infolist()
                myzip.extractall()
            for i in z:
                if i.filename != 'key':
                    in_filename = i.filename
            print "in decrypt func", in_filename
            f1 = open("key", "rb")
            input = f1.read()
            f1.close()
            pri_key = RSA.importKey(private_key)
            key = pri_key.decrypt(input)
            out_filename = in_filename.strip('.enc')
            print key
            with open(in_filename, 'rb') as infile:
                print "Inside with in_filname"
                origsize = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
                iv = infile.read(16)
                print "iv : " + iv
                try:
                    decryptor = AES.new(key, AES.MODE_CBC, iv)
                    print decryptor
                except Exception as e:
                        return -1        
                chunksize=24*1024
                with open(out_filename, 'wb') as outfile:
                    print out_filename
                    while True:
                        chunk = infile.read(chunksize)
                        #print chunk
                        if len(chunk) == 0:
                            break
                        outfile.write(decryptor.decrypt(chunk))
                    outfile.truncate(origsize)

            fh = open(out_filename,'rb')
            response = HttpResponse(fh.read(), content_type="application/zip")
            response['Content-Disposition'] = 'inline; filename=%s'%out_filename
            return response
    except:
        pass

